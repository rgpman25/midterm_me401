// ME 401 Midterm project
// Group 19
//


// States: 
//      Healing
//          - Use global data to drive to a zombie 
//      Zombie
//          - find other robots and touch/attack based only on distance sensor
//      healthy
//          - avoid zombies using global data 
//          - pick up ball if no zombies
//
// Transitions: 
//      healthy -> healing : pick up ball 
//      healing -> healthy : healed someone
// 
//      healthy -> zombie : touched by zombie
//      zombie -> healthy : healed
//

#include "ME401_Radio.h"
//#include "ME401_PID.h"

#define MY_ROBOT_ID 19

enum stateMachineState 
{
    UNKNOWN = 0, 
    HEALING = 1,
    HEALTHY = 2,
    ZOMBIE = 3,
};

// Switch that we nosed into something
int contactSwitch = 6;

// Set default state
stateMachineState STATE = HEALTHY;

void setup(){
    Serial.begin(115200);

    ME401_Radio_initialize();
    
   // setupPIDandIR();


}

void loop(){
    //Serial.print("millis:");
   // Serial.println(millis()); 

// Check state and then execute each state code
//    checkState();
    switch(STATE){
        case (HEALTHY): 
            healthyState();
            break;
        case (HEALING):
            healingState();
            break;
        case (ZOMBIE):
            zombieState();
            break;
        case (UNKNOWN):
            unknownState();
            break;
    }




delay(100);
}

void checkState(){
  updateRobotPoseAndBallPositions();
  RobotPose robot = getRobotPose(MY_ROBOT_ID);
 

  if (robot.valid == true)
  {
    if (robot.zombie){
        STATE = ZOMBIE;
    }
    else if (robot.healing){
        STATE = HEALING;
    }
    else if(!robot.healing && !robot.zombie){
        STATE = HEALTHY;
    }
    else{
        STATE = UNKNOWN;
    }
  }
}

void zombieState(){
// only use distance sensor to find targets
    Serial.println("In ZOMBIE State");
// check if healed == 0 -> 

checkState();
}

void healingState(){

    Serial.println("In HEALING State");

    int target_ID = find_closest_zombie();

    RobotPose target_bot = getRobotPose(target_ID);
    
    drive_to(target_bot.x, target_bot.y);
    

//      healing -> healthy : healed someone
//      healing -> zombie : healed someone -> touched by zombie

checkState();
}

void healthyState(){

    Serial.println("In HEALTHY state");
//      healthy -> healing : pick up ball 
//      healthy -> zombie : touched by zombie

checkState();
}

void unknownState(){
    Serial.println("UNKNOWN STATE");
    checkState();
}

int checkBoop(){
    return digitalRead(contactSwitch) == 1;
}




int find_closest_zombie(){

  RobotPose robot = getRobotPose(MY_ROBOT_ID); //do we need this in each function?
  
  int ID = -1;
  int min_dist = 10000;

  
  for (int i = 1; i <= getNumRobots()){
      
      RobotPose current = getRobotPose(i); //wont work because ID's present will be random. Need to read the ID's present and skip our own ID
      int dist = abs_dist(robot.x, robot.y, current.x, current.y);

      if(dist < min_dist){
        min_dist = dist;
        ID = current.ID;
      }
     
  }
  
//return robot id of closest zombie
  return ID;
}

void drive_to(int x, int y){
 
  RobotPose robot = getRobotPose(MY_ROBOT_ID);
  
  //motor controls to drive to x y coordinates
  
  
  //1. Align angle to point towards the point
  int des_angle = tan((x-robot.x)/(y-robot.y)); //tan will provide angle in radians, we want degrees
  des_angle = degrees(des_angle);
  
  //convert robot.theta to the corresponding angle between 0 and 360
  int current_angle = robot.theta % 360;
  if(current_angle < 0){
    current_angle = current_angle + 360;
  }

  diff = current_angle - des_angle;

  if(abs(diff) > 5){
    if(((diff > 0) && (diff >=180))||((diff < 0) && (diff >= -180))){
    //rotate clockwise until reach desired angle
    
    }
    else{
    //rotate counterclockwise until reach desired angle
    
    }
  }
  
  
  //2. Full speed ahead until close 
  //maybe only use the angle measure 
  //if (abs_dist(x, y, robot.x, robot.y) > 10){
    //go to point as fast as possible
//    ls = map(l_speed, -100, 100, 1300, 1700);
//    leftservo.writeMicroseconds(ls);
//    rs = map(l_speed, -100, 100, 1300, 1700);
//    rightservo.writeMicroseconds(rs);
  }
}

int abs_dist(x1, y1, x2, y2){
  return sqrt(sq(x1-x2)+sq(y1-y2));
}




